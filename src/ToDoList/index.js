import React, { PureComponent } from 'react';
import Header from './Header'
import ToDoList from './ToDoList'
import SubmitForm from './SubmitForm'
import styled from 'styled-components';

const Wrapper = styled.section`
  padding: 5em;
  background: papayawhip;
`;

class Routes extends PureComponent {
    constructor() {
        super();
        this.state = {
            tasks: [
                {
                    name: 'task 1',
                    edited: false
                },
                {
                    name: 'task 2',
                    edited: false
                },
                {
                    name: 'task 3',
                    edited: false
                }
            ]
        };
    }

    handleEdit = (index) => {
        const newArr = [...this.state.tasks];
        newArr[index].edited = true;
        this.setState({ tasks: newArr });
        console.log(newArr);
    }

    handleUpdate = (index, term) => {
        const newArr = [...this.state.tasks];
        newArr[index].name = term;
        newArr[index].edited = false;
        this.setState({ tasks: newArr });
    }

    handleDelete = (index) => {
        const newArr = [...this.state.tasks];
        newArr.splice(index, 1);
        this.setState({ tasks: newArr });
    }

    handleSubmit = task => {
        console.log(task);
        this.setState({ tasks: [...this.state.tasks, { name: task, edited: false }] });
    }

    render() {
        return (
            <Wrapper>
                <div className='card frame'>
                    <Header numTodos={this.state.tasks.length} />
                    <SubmitForm onFormSubmit={this.handleSubmit} />
                    <ToDoList tasks={this.state.tasks} onDelete={this.handleDelete}
                        onUpdate={this.handleUpdate} onEdit={this.handleEdit} />
                </div>
            </Wrapper>
        )
    }
};

export default Routes;