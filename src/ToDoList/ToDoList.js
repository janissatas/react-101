import React from 'react';
import ToDo from './ToDo';

const ToDoList = (props) => {
    const todos = props.tasks.map((todo, index) => {
      //console.log({todo});
      return <ToDo content={todo.name} edited={todo.edited} key={index} id={index} onEdit={props.onEdit} 
              onDelete={props.onDelete} onUpdate={props.onUpdate}/>
    })
    return( 
      <div className='list-wrapper'>
        {todos}
      </div>
    );
  }

export default ToDoList;