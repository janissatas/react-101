import React from 'react';
import styled, { css } from 'styled-components';

const Button = styled.button`
  /* Adapt the colors based on primary prop */
  background: ${props => props.primary ? "palevioletred" : "white"};
  color: ${props => props.primary ? "white" : "palevioletred"};

  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;

  /* The GitHub button is a primary button
   * edit this to target it specifically! */
  ${props => props.primary && css` /* props => props.primary ? css'background: white' : css'background: black'*/
    display: ${props => props.edited ? "inline" : "none"};
  `}

  ${props => props.edit && css` /* props => props.primary ? css'background: white' : css'background: black'*/
    display: ${props => props.edited ? "none" : "inline"};
  `}
`

const Div = styled.div`
  display: ${props => props.edited ? "none" : "inline"};
`;

const Input = styled.input`
  display: ${props => props.edited ? "inline" : "none"};
`;

class ToDo extends React.Component {

  constructor(props){
    super(props.content);
    this.state = {
        term: props.content
    }
  }

  render(){

    const { edited } = this.props;
    return(
      <div className='list-item'>
        <Div edited={edited}>
          {this.props.content}
        </Div>
        <Input
          type='text'
          className='input'
          placeholder='Enter Item'
          value={this.state.term}
          edited={edited}
          onChange={(e) => this.setState({term: e.target.value})}
        />
        <Button onClick={() => {this.props.onEdit(this.props.id)}} edit edited={edited}> Edit </Button>
        <Button onClick={() => {this.props.onUpdate(this.props.id, this.state.term)}} primary edited={edited}> Save </Button>
        <Button onClick={() => {this.props.onDelete(this.props.id)}} > Delete </Button>
      </div>
    );
  }

}

export default ToDo